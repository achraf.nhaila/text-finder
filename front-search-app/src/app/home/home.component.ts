import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SearchService } from '../service/search.service';
import { Router } from '@angular/router';
import { Data } from '../service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  servers: any;
  searchForm: FormGroup;
  loading: boolean = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
    private searchService: SearchService,
    private router: Router,
    private data: Data) {
  }

  get f() { return this.searchForm.controls; }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      server: ['', Validators.required],
      keyWord: ['', Validators.required],
      path: ['', Validators.required],
    });

    this.searchService.getAll()
    this.loading = true;
    this.searchService.getAll().subscribe(data => {
      this.loading = false;
      this.servers = data
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.searchForm.invalid) {
      return;
    }

    this.data.storage = this.searchForm.value;
    this.router.navigate(["result-list"]);
  }

  onReset() {
    this.submitted = false;
    this.searchForm.reset();
  }
}
