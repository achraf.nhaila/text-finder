import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class WebSocketAPI {
    webSocketEndPoint: string = 'http://localhost:8777/ws';
    topic: string = "/result/file";
    stompClient: any;
    resultListComponent: any;

    constructor(resultListComponent: any) {
        this.resultListComponent = resultListComponent;
    }

    _connect() {
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function (frame) {
            _this.resultListComponent.sendMessage();
            _this.stompClient.subscribe(_this.topic, function (sdkEvent) {
                _this.onMessageReceived(sdkEvent);
            });
            _this.stompClient.reconnect_delay = 2000;
        }, this.errorCallBack);
    }

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    errorCallBack(error: any) {
        console.log("errorCallBack -> " + error)
    }

    _send(message: any) {
        console.log("calling logout api via web socket");
        this.stompClient.send("/app/search", {}, JSON.stringify(message));
    }

    onMessageReceived(message: any) {
        this.resultListComponent.handleMessage(message.body);
    }
}
