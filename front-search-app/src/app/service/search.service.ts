import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class SearchService {
  public API = '//localhost:8777/api';
  public SEARCH_API = this.API + '/search';
  public SERVERS_API = this.API + '/servers';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(this.SERVERS_API);
  }

  search(input: any): Observable<any> {
    return this.http.post(this.SEARCH_API, input);
  }
}
