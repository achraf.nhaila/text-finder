export class InputData {
    path: string;
    keyWord: string;
    servers: string[];

    constructor(inputData: any) {
        {
            this.path = inputData.path;
            this.keyWord = inputData.keyWord;
            this.servers = inputData.servers
        }
    }
}
