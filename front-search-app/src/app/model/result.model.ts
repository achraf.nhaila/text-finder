export class FileResult {
    fileName: string;
    counter: number;
    resideServer: string;

    constructor(fileResult: any) {
        this.fileName = fileResult.fileName;
        this.counter = fileResult.counter;
        this.resideServer = fileResult.resideServer

    }

    public get _fileName(): string {
        return this.fileName;
    }

    public get _counter(): number {
        return this.counter;
    }

    public get _resideServer(): string {
        return this.resideServer;
    }
}
