export class StatusResponse {
    startTime: string;
    endTime: string;
    durationSeconds: number;

    constructor(statusResponse: any) {
        {
            this.startTime = statusResponse.startTime;
            this.endTime = statusResponse.endTime;
            this.durationSeconds = statusResponse.durationSeconds
        }
    }
}
