import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { InputData } from '../model/input.model';
import { Data } from '../service/data.service';
import { Router } from '@angular/router';
import { StatusResponse } from '../model/statusresponse.model';
import { WebSocketAPI } from '../service/websocketapi.service';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.css']
})
export class ResultListComponent implements OnInit {
  displayedColumns: string[] = ['fileName', 'resideServer', 'counter'];
  results: Array<any> = [];
  webSocketAPI: WebSocketAPI;

  statusResponse: StatusResponse;
  input: InputData;
  isCompleted: boolean;

  constructor(private cdr: ChangeDetectorRef,
    private router: Router,
    private data: Data) {
    this.input = data.storage;
  }

  ngOnInit() {
    this.webSocketAPI = new WebSocketAPI(this);
    this.webSocketAPI._connect();
    this.isCompleted = false;
  }

  sendMessage() {
    if (this.input) {
      this.isCompleted = true;
      this.webSocketAPI._send(this.input);
    } else {
      this.router.navigate(["home"]);
    }
  }

  ngOnDestroy() {
    this.webSocketAPI._disconnect();
  }

  handleMessage(message: any) {
    if (message.includes('startTime')) {
      this.statusResponse = JSON.parse(message);

      if (this.statusResponse.endTime) {
        this.webSocketAPI._disconnect();
        this.isCompleted = false;
      }
    } else {
      this.results.push(JSON.parse(message));
    }
  }
}
