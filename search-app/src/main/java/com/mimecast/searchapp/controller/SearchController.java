/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.controller;

import com.google.common.base.Strings;
import com.mimecast.searchapp.config.ServersSetting;
import com.mimecast.searchapp.dto.InputDto;
import com.mimecast.searchapp.dto.ServerDto;
import com.mimecast.searchapp.dto.StatusResponseDto;
import com.mimecast.searchapp.services.ManagerService;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static com.mimecast.searchapp.constants.Constants.DATE_PATTERN;
import static com.mimecast.searchapp.constants.Constants.SUBSCRIBE_SEARCH_ENDPOINT;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Web service which expose two rest api :
 * Get list of servers.
 * Perform the search wih criteria within the servers chosen
 */
@RestController
public class SearchController {
    private final ServersSetting serversSetting;
    private final ManagerService managerService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);

    public SearchController(ServersSetting serversSetting,
                            ManagerService managerService,
                            SimpMessagingTemplate simpMessagingTemplate) {
        this.serversSetting = serversSetting;
        this.managerService = managerService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @GetMapping("/api/servers")
    @CrossOrigin
    public ResponseEntity<List<ServerDto>> getServers() {
        List<ServerDto> servers = serversSetting.getServers().entrySet().stream()
                .map(s -> new ServerDto(s.getKey(), s.getValue().getName()))
                .collect(Collectors.toList());

        return ResponseEntity.ok(servers);
    }

    @MessageMapping("/search")
    public void doSearch(InputDto inputDto) {
        // send start time
        final LocalDateTime from = LocalDateTime.now();
        StatusResponseDto status = new StatusResponseDto(from.format(formatter), null, null);
        simpMessagingTemplate.convertAndSend(SUBSCRIBE_SEARCH_ENDPOINT, status);

        // execute
        if (!Strings.isNullOrEmpty(inputDto.getKeyWord())) {
            managerService.execute(inputDto);
        }

        final LocalDateTime end = LocalDateTime.now();
        final Long seconds = SECONDS.between(from, end);

        // Send end time
        status = new StatusResponseDto(from.format(formatter), end.format(formatter), seconds);
        simpMessagingTemplate.convertAndSend(SUBSCRIBE_SEARCH_ENDPOINT, status);
    }
}
