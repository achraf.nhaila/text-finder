/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.controller;

import com.mimecast.searchapp.exceptions.ReadFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *  Controller to declare handling exceptions methods to be shared across multiple {@code @Controller} classes.
 *  I can declare much as i can here
 */
@ControllerAdvice
public class RestErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestErrorHandler.class);

    @ExceptionHandler(ReadFileException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handleReadFileException(ReadFileException ex) {
        LOGGER.debug("handling reading file");
    }
}
