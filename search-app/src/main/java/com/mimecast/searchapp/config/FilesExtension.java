/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "mimecast")
public class FilesExtension {
    private List<String> filesExtension;

    public List<String> getFilesExtension() {
        return Collections.unmodifiableList(filesExtension);
    }

    public void setFilesExtension(List<String> filesExtension) {
        this.filesExtension = filesExtension;
    }
}
