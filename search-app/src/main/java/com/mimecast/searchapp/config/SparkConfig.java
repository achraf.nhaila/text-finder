/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.config;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

@Configuration
public class SparkConfig implements Serializable {
    @Value("${mimecast.spark.appname}")
    private String appName;
    @Value("${mimecast.spark.masteruri}")
    private String masterUri;

    @Bean
    public SparkConf sparkConf() {
        SparkConf sparkConf = new SparkConf()
                .setAppName(appName)
                .setMaster(masterUri)
                .set("spark.ui.enabled", "false");

        return sparkConf;
    }

    @Bean
    public JavaSparkContext sparkContext() {
        return new JavaSparkContext(sparkConf());
    }
}
