/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadConfig {
    @Value("${mimecast.threadpool.corepoolsize}")
    private int corePoolSize;

    @Value("${mimecast.threadpool.maxpoolsize}")
    private int maxPoolSize;

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(corePoolSize);
        pool.setMaxPoolSize(maxPoolSize);
        pool.setThreadNamePrefix("default_task_executor_thread");
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }
}
