/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.config;

import com.mimecast.searchapp.dto.ServerCredentialDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "mimecast")
public class ServersSetting {
    private Map<String, ServerCredentialDto> servers;

    public Map<String, ServerCredentialDto> getServers() {
        return servers;
    }

    public void setServers(Map<String, ServerCredentialDto> servers) {
        this.servers = servers;
    }
}
