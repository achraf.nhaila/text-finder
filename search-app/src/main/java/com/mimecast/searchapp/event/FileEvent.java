/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.event;

import org.springframework.context.ApplicationEvent;

/**
 * class represent event to fire when file ready to be processed
 */
public class FileEvent extends ApplicationEvent {
    private String path;
    private String keyword;
    private String server;

    public FileEvent(Object source, final String path, final String keyword, final String server) {
        super(source);
        this.path = path;
        this.keyword = keyword;
        this.server = server;
    }

    /**
     * Get path
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     * get key word
     * @return
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * get reside server
     * @return
     */
    public String getServer() {
        return server;
    }
}
