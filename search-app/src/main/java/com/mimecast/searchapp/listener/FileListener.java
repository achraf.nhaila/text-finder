/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.listener;

import com.mimecast.searchapp.event.FileEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/*I want to perform an async execution using the listener but i need to track the execution time*/
@Component
public class FileListener {
    private final TaskExecutor taskExecutor;
    private final ApplicationContext applicationContext;
    public FileListener(TaskExecutor taskExecutor, ApplicationContext applicationContext) {
        this.taskExecutor = taskExecutor;
        this.applicationContext = applicationContext;
    }

    @EventListener
    public void handleFileEvent(FileEvent fileEvent) {
    }
}
