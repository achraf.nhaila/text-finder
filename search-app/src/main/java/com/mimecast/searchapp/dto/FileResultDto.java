/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.dto;

import java.io.Serializable;

/**
 * Class which represents the result found for each file
 */
public class FileResultDto implements Serializable {
    private String fileName;
    private Long counter;
    private String resideServer;

    public FileResultDto() {
    }

    public FileResultDto(String fileName, Long counter, String resideServer) {
        this.fileName = fileName;
        this.counter = counter;
        this.resideServer = resideServer;
    }

    /**
     * Get file name
     * @return file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Get counter of word
     * @return counter
     */
    public Long getCounter() {
        return counter;
    }

    /**
     * get reside server
     * @return reside server
     */
    public String getResideServer() {
        return resideServer;
    }

    /**
     * set file name
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * set counter
     * @param counter
     */
    public void setCounter(Long counter) {
        this.counter = counter;
    }

    /**
     * set reside server
     * @param resideServer
     */
    public void setResideServer(String resideServer) {
        this.resideServer = resideServer;
    }
}
