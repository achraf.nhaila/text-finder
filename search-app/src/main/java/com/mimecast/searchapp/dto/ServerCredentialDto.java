/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.dto;

import java.io.Serializable;

/**
 * Dto which represents information needed for connection to remote server
 */
public class ServerCredentialDto implements Serializable {
    private String name;
    private String server;
    private int port;
    private String username;
    private String password;
    private int keepAliveTimeout;

    /**
     * get name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get server
     * @return
     */
    public String getServer() {
        return server;
    }

    /**
     * set server
     * @param server
     */
    public void setServer(String server) {
        this.server = server;
    }

    /**
     * get port
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * set port
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * get user name
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * set user name
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * get pass word
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * set pass word
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * get keep alive timeout value
     * @return
     */
    public int getKeepAliveTimeout() {
        return keepAliveTimeout;
    }

    /**
     * set keep alive time out value
     * @param keepAliveTimeout
     */
    public void setKeepAliveTimeout(int keepAliveTimeout) {
        this.keepAliveTimeout = keepAliveTimeout;
    }
}
