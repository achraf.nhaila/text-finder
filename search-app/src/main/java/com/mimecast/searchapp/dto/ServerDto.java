/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class represents servers which the client could choose in order to perform the search within them
 */
public class ServerDto implements Serializable {
    private String name;
    private String value;

    public ServerDto() {
    }

    public ServerDto(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * get name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set name of server
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get value of server
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * set value of server
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerDto serverDto = (ServerDto) o;
        return Objects.equals(name, serverDto.name) &&
                Objects.equals(value, serverDto.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }
}
