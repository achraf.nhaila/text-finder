/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.dto;

import java.io.Serializable;

/**
 * Dto represents the status of search execution at the end
 */
public class StatusResponseDto implements Serializable {
    private String startTime;
    private String endTime;
    private Long durationSeconds;

    public StatusResponseDto() {
    }

    public StatusResponseDto(String startTime, String endTime, Long durationSeconds) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.durationSeconds = durationSeconds;
    }

    /**
     * Get start time
     * @return start time
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * get end time
     * @return end time
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * get duration of execution by seconds
     * @return execution duration
     */
    public Long getDurationSeconds() {
        return durationSeconds;
    }
}
