/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.dto;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Dto for input search
 */
public class InputDto implements Serializable {
    private String path;
    private String keyWord;
    private List<String> server;

    private InputDto() {
    }

    /**
     * Get path to search in
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * set path to search in
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * get key word
     * @return key word
     */
    public String getKeyWord() {
        return Strings.isNullOrEmpty(keyWord) ? null : keyWord.trim();
    }

    /**
     * set key word
     * @param keyWord
     */
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    /**
     * get servers
     * @return
     */
    public List<String> getServer() {
        return Collections.unmodifiableList(server);
    }

    /**
     * set servers
     * @param servers
     */
    public void setServer(List<String> servers) {
        this.server = servers;
    }

    /**
     * Builder for Input data input
     */
    public static class Builder {
        private String path;
        private String keyWord;
        private List<String> server;

        public Builder() {
            this.server = Lists.newArrayList();
        }

        public Builder withServer(List<String> server) {
            this.server = server;
            return this;
        }

        public Builder withPath(String path) {
            this.path = path;
            return this;
        }

        public Builder withKeyWord(String keyWord) {
            this.keyWord = keyWord;
            return this;
        }

        public InputDto build() {
            InputDto inputDto = new InputDto();
            inputDto.setServer(this.server);
            inputDto.setPath(this.path);
            inputDto.setKeyWord(this.keyWord);

            return inputDto;
        }
    }
}
