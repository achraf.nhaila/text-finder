/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.exceptions;

public class ExecutionException extends RuntimeException {
    public ExecutionException(String message) {
        super(message);
    }
}
