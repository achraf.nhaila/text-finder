/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.exceptions;

/**
 * Read file exception
 */
public class ReadFileException extends RuntimeException {
    public ReadFileException(String message) {
        super(message);
    }
}
