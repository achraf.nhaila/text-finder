/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.exceptions;

/**
 * Ftp exception
 */
public class FtpException extends RuntimeException {
    public FtpException(String message) {
        super(message);
    }
}
