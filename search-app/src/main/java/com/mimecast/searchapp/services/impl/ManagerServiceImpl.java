package com.mimecast.searchapp.services.impl;

import com.google.common.collect.Lists;
import com.mimecast.searchapp.config.ServersSetting;
import com.mimecast.searchapp.dto.InputDto;
import com.mimecast.searchapp.dto.ServerCredentialDto;
import com.mimecast.searchapp.exceptions.ExecutionException;
import com.mimecast.searchapp.services.ConsumerService;
import com.mimecast.searchapp.services.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

@Service
public class ManagerServiceImpl implements ManagerService {
    private Logger logger = LoggerFactory.getLogger(ManagerServiceImpl.class);

    private final ServersSetting serversSetting;
    private final ApplicationContext ctx;
    private final ThreadPoolTaskExecutor taskExecutor;

    public ManagerServiceImpl(ServersSetting serversSetting,
                              ThreadPoolTaskExecutor taskExecutor,
                              ApplicationContext ctx) {
        this.serversSetting = serversSetting;
        this.taskExecutor = taskExecutor;
        this.ctx = ctx;
    }

    /**
     * Execute the search in each server
     * @param input search criteria
     * @return status of result
     */
    @Override
    public Boolean execute(final InputDto input) {
        List<Future<Boolean>> futureExecutions = Lists.newArrayList();
        for(int i = 0; i < input.getServer().size(); i++) {
            Optional<ServerCredentialDto> server = Optional.ofNullable(serversSetting.getServers()
                    .get(input.getServer().get(i)));

            final int threadNumber = i;
            server.ifPresent(s -> {
                ConsumerService callableTask = ctx.getBean(ConsumerService.class, server.get(), input.getKeyWord(), input.getPath(), String.valueOf(threadNumber));
                futureExecutions.add(taskExecutor.submit(callableTask));
            });
        }

        Boolean executionStatus = true;
        for(Future<Boolean> future: futureExecutions){
            try {
                if (!future.get()) {
                    executionStatus = false;
                }
            } catch (Exception e) {
                logger.error("", e);

                throw new ExecutionException("Execution error");
            }
        }

        return executionStatus;
    }
}
