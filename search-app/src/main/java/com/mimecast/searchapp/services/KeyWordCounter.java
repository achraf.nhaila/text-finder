package com.mimecast.searchapp.services;

public interface KeyWordCounter {
    void executeFor(final String path, final String keyword, final String server);
}
