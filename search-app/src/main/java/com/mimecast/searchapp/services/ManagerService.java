package com.mimecast.searchapp.services;

import com.mimecast.searchapp.dto.InputDto;

public interface ManagerService {
    Boolean execute(InputDto input);
}
