/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.services;

import com.mimecast.searchapp.config.FilesExtension;
import com.mimecast.searchapp.constants.Constants;
import com.mimecast.searchapp.dto.ServerCredentialDto;
import com.mimecast.searchapp.exceptions.ReadFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Callable;

import static org.apache.commons.io.FilenameUtils.getExtension;

@Service
@Scope("prototype")
public class ConsumerService implements Callable<Boolean> {
    private final Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    @Autowired
    private KeyWordCounter keyWordCounter;
    @Autowired
    private FilesExtension filesExtension;

    private final ServerCredentialDto server;
    private final String keyWord;
    private final String path;
    private final String name;

    public ConsumerService(ServerCredentialDto server, String keyWord, String path, String name) {
        this.server = server;
        this.keyWord = keyWord;
        this.path = path;
        this.name = name;
    }


    /**
     * Walks a folder tree this is for local testing purpose.
     *
     * We can do it for remote server over ftp or sftp ......
     *
     * The idea is to implement our logic in order to construct url of files within the server {@link this.server},
     * at the end trying to have some sort of tree folder which is provided as parameter.
     * We can use the library {@link http://epaul.github.io/jsch-documentation/javadoc/} to perform the <i>ls<i/>
     * method and the construct the tree.
     *
     * At the end we can supply spark with url like <p>ftp://domain/myTest/test.txt</p>
     *
     * @return
     */
    @Override
    public Boolean call() {
        Path path = Paths.get(this.path);
        try {
            Files.walkFileTree(path, new CustomSimpleFileVisitor());
        } catch (IOException e) {
            logger.error("" + e);

            throw new ReadFileException("I/O error walking through folder " + path.toString());
        }

        return true;
    }

    /**
     *  A simple visitor of files with default behavior to visit all files and to
     *  catch some I/O errors.
     */
    private class CustomSimpleFileVisitor extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            final Boolean supportedFile = filesExtension.getFilesExtension().contains(getExtension(file.toString()));
            if (!attrs.isDirectory() && Files.isReadable(file) && supportedFile
                    && !isNotFileNameValid(file.toString())) {
                // Search for key word within a file
                keyWordCounter.executeFor(file.toString(), keyWord, server.getName());
            }

            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException e) {
            return FileVisitResult.SKIP_SUBTREE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            return FileVisitResult.CONTINUE;
        }

        private boolean isNotFileNameValid(final String fileName) {
            for (int i = 0; i < Constants.ILLEGAL_CHARACTERS.length; i++) {
                if (fileName.indexOf(Constants.ILLEGAL_CHARACTERS[i]) > -1) {
                    return true;
                }
            }
            return false;
        }
    }
}
