package com.mimecast.searchapp.services.impl;

import com.mimecast.searchapp.dto.FileResultDto;
import com.mimecast.searchapp.services.KeyWordCounter;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static com.mimecast.searchapp.constants.Constants.SUBSCRIBE_SEARCH_ENDPOINT;

@Service
public class KeyWordCounterImpl implements KeyWordCounter {
    private Logger logger = LoggerFactory.getLogger(KeyWordCounterImpl.class);

    private final JavaSparkContext sparkContext;
    private final SimpMessagingTemplate simpMessagingTemplate;

    public KeyWordCounterImpl(JavaSparkContext sparkContext, SimpMessagingTemplate simpMessagingTemplate) {
        this.sparkContext = sparkContext;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    /**
     * Search for a key word in file with spark session
     * @param path
     * @param keyword
     * @param server
     */
    @Override
    public void executeFor(final String path, final String keyword, final String server) {
        if (Files.notExists(Paths.get(path))) {
            return;
        }

        try {
            final Long counter = sparkContext.textFile(path)
                    .flatMap(f -> Arrays.asList(f.split("\\W+")).iterator())
                    .filter(f -> f.equalsIgnoreCase(keyword))
                    .count();

            if (counter != null && !counter.equals(Long.valueOf(0))) {
                simpMessagingTemplate.convertAndSend(SUBSCRIBE_SEARCH_ENDPOINT, new FileResultDto(path, counter, server));
            }
        } catch (Exception ex) {
            logger.error("This file can't be processed : " + path);
        }
    }
}
