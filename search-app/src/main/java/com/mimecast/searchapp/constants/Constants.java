/*
 * Copyright (c) 2020. The search api
 */

package com.mimecast.searchapp.constants;

/**
 * Class which represents the constants values used across the application
 */
public class Constants {
    /*web socket*/
    public static final String SEND_SEARCH_ENDPOINT = "/app/search";
    public static final String SUBSCRIBE_SEARCH_ENDPOINT = "/result/file";

    /*Time out*/
    public static final Long TIME_OUT = 1000L;

    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /*Ignore processing files having one or more illegal chars in the name*/
    public static final char[] ILLEGAL_CHARACTERS = {'`', '?', '*', '<', '>', '|', '\"', ',' };
}
