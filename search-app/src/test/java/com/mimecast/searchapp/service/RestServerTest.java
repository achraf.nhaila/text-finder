package com.mimecast.searchapp.service;

import com.mimecast.searchapp.config.ServersSetting;
import com.mimecast.searchapp.dto.ServerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestServerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ServersSetting serversSetting;

    @Test
    public void verifyGetServersIsReceived() {
        // Given
        List<ServerDto> expected = serversSetting.getServers().entrySet().stream()
                .map(s -> new ServerDto(s.getKey(), s.getValue().getName()))
                .collect(Collectors.toList());

        // Then
        List<ServerDto> result = restTemplate.getForObject(getRestPath(), ArrayList.class);

        assertNotNull(result);
        assertEquals(result.size(), expected.size());
    }

    private String getRestPath() {
        return String.format("http://localhost:%d/api/servers", port);
    }
}
