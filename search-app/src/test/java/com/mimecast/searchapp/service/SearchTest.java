package com.mimecast.searchapp.service;

import com.google.common.collect.Lists;
import com.mimecast.searchapp.dto.FileResultDto;
import com.mimecast.searchapp.dto.InputDto;
import com.mimecast.searchapp.dto.StatusResponseDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.mimecast.searchapp.constants.Constants.SEND_SEARCH_ENDPOINT;
import static com.mimecast.searchapp.constants.Constants.SUBSCRIBE_SEARCH_ENDPOINT;
import static com.mimecast.searchapp.constants.Constants.TIME_OUT;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SearchTest {
    @LocalServerPort
    private int port;

    private WebSocketStompClient webSocketStompClient;
    private CompletableFuture completableFuture;

    @BeforeEach
    public void setup() {
        this.completableFuture = new CompletableFuture<>();
        this.webSocketStompClient = new WebSocketStompClient(new SockJsClient(
                Lists.newArrayList(new WebSocketTransport(new StandardWebSocketClient()))));
    }

    @Test
    public void verifySearchEndIsReceived(@TempDir Path tempDir) throws Exception {
        webSocketStompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession session = webSocketStompClient
                .connect(getWsPath(), new StompSessionHandlerAdapter() {})
                .get(TIME_OUT, SECONDS);

        session.subscribe(SUBSCRIBE_SEARCH_ENDPOINT, new CreateSearchEndStompFrameHandler());

        InputDto input = new InputDto.Builder()
                .withKeyWord("and")
                .withPath(tempDir.toString())
                .withServer(Lists.newArrayList("firstServer"))
                .build();

        session.send(SEND_SEARCH_ENDPOINT, input);

        StatusResponseDto status = (StatusResponseDto) completableFuture.get(TIME_OUT, SECONDS);

        assertNotNull(status);
    }

    @Test
    public void verifySearchIsReceived(@TempDir Path tempDir) throws Exception {
        Path words = tempDir.resolve("words.txt");

        List<String> lines = Arrays.asList("and", "or", "and", "and", "and tata");
        Files.write(words, lines);

        assertAll(
                () -> assertTrue("File should exist", Files.exists(words)),
                () -> assertLinesMatch(lines, Files.readAllLines(words)));

        webSocketStompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession session = webSocketStompClient
                .connect(getWsPath(), new StompSessionHandlerAdapter() {})
                .get(TIME_OUT, SECONDS);
        session.subscribe(SUBSCRIBE_SEARCH_ENDPOINT, new CreateSearchStompFrameHandler());

        InputDto input = new InputDto.Builder()
                .withKeyWord("and")
                .withPath(tempDir.toString())
                .withServer(Lists.newArrayList("firstServer"))
                .build();
        session.send(SEND_SEARCH_ENDPOINT, input);

        // Response
        FileResultDto result = (FileResultDto) completableFuture.get(TIME_OUT, SECONDS);

        assertAll(
                () -> assertNotNull(result));
    }

    private String getWsPath() {
        return String.format("ws://localhost:%d/ws", port);
    }

    private class CreateSearchEndStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return StatusResponseDto.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            completableFuture.complete(o);
        }
    }

    private class CreateSearchStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return FileResultDto.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            completableFuture.complete(o);
        }
    }
}
