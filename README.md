## Development server
Run `npm install`
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Normal Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.




## This for docker but the server side also should be known to get the communication between two sides




## Docker build

Run `docker build -t front-search .` 

## Check the list of locally available images

Run `docker image ls`

## Run the container

Run `docker run --name front-search-container -d -p 8081:80 front-search`

## Check the container is up

Run `docker container ls`

## Enter in the web browser and go to http://localhost:8081/
